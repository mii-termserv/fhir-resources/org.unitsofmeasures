# UCUM Placeholder Package

UCUM is a nomenclature for generating machine-readable, unambigous expressions for coding units of measures. It is not possible to represent UCUM in its entirety as a FHIR CodeSystem. As of writing, this package only contains a placeholder CodeSystem resource with the FHIR canonical http://unitsofmeasure.org

At a future point in time, this will be replaced to include a subset of commonly-used codes from UCUM.