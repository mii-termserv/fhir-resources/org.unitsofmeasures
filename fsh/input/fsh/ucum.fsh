Alias: $sutermserv_project = https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-project
Alias: $sutermserv_dataset = https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-dataset
Alias: $sutermserv_license = https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-license

CodeSystem: UCUM
Id: ucum
Title: "Unified Code for Units of Measure (UCUM)"
Description: "The Unified Code for Units of Measure (UCUM) is a code system intended to include all units of measures being contemporarily used in international science, engineering, and business. The purpose is to facilitate unambiguous electronic communication of quantities together with their units. The focus is on electronic communication, as opposed to communication between humans. A typical application of The Unified Code for Units of Measure are electronic data interchange (EDI) protocols, but there is nothing that prevents it from being used in other types of machine communication."
* ^url = "http://unitsofmeasure.org"
* ^version = "2024.7.1"
* ^identifier[+].system = "urn:ietf:rfc:3986"
* ^identifier[+].value = "urn:oid:2.16.840.1.113883.6.8"
* ^meta.tag[+].system = "$sutermserv_project"
* ^meta.tag[=].code = #international
* ^meta.tag[=].display = "International standard terminology"
* ^meta.tag[+].system = "$sutermserv_dataset"
* ^meta.tag[=].code = #ucum
* ^meta.tag[=].display = "Versions of SNOMED CT"
* ^meta.tag[+].system = "$sutermserv_license"
* ^meta.tag[=].code = #https://mii-termserv.de/licenses#ucum
* ^meta.tag[=].display = "UCUM license"
* ^extension[+].url = "http://hl7.org/fhir/StructureDefinition/cqf-scope"
* ^extension[=].valueString = "@mii-termserv/org.unitsofmeasures"
* ^status = #active
* ^experimental = false
* ^date = "2024-07-01"
* ^publisher = "LOINC and Health Data Standards, Regenstrief Institute, Inc."
* ^copyright = "The UCUM codes, UCUM table (regardless of format), and UCUM Specification are copyright 1999-2009, Regenstrief Institute, Inc. and the Unified Codes for Units of Measures (UCUM) Organization. All rights reserved. https://ucum.org/trac/wiki/TermsOfUse"
* ^caseSensitive = true
* ^content = #not-present